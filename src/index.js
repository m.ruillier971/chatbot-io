// eslint-disable-next-line max-classes-per-file
import './index.scss';
import env from '../env';
// eslint-disable-next-line import/no-extraneous-dependencies
const { Configuration, OpenAIApi } = require('openai');

const STORAGE_KEY = '19051994';

const configuration = new Configuration({
  // eslint-disable-next-line no-undef
  apiKey: env.OPENAI_API_KEY
});
const openai = new OpenAIApi(configuration);

const Bot = class {
  constructor(type, name, date, image, text) {
    this.type = type;
    this.name = name;
    this.date = date;
    this.image = image;
    this.text = text;
  }

  get getName() {
    return this.name;
  }

  set setName(newName) {
    this.name = newName;
  }

  get getDate() {
    return this.date;
  }

  set setDate(newDate) {
    this.data = newDate;
  }

  get getImage() {
    return this.image;
  }

  set setImage(newImage) {
    this.image = newImage;
  }

  get getText() {
    return this.text;
  }

  set setText(newText) {
    this.text = newText;
  }

  async generateChatGPT(input) {
    const response = await openai.createCompletion({
      model: 'text-davinci-003',
      prompt: input,
      temperature: 0,
      max_tokens: 50
    });
    return response.data.choices[0].text.trim();
  }

  async fetchCocktailRecipe() {
    return fetch('https://www.thecocktaildb.com/api/json/v1/1/random.php', {
      headers: {
        Accept: 'application/json'
      }
    }).then((data) => data.text()).then((data) => JSON.parse(data));
  }

  async fetchJoke() {
    return fetch('https://v2.jokeapi.dev/joke/Any?lang=en', {
      headers: {
        Accept: 'application/json'
      }
    }).then((data) => data.text()).then((data) => JSON.parse(data));
  }
};

const hubertBot = new Bot('hubertBot', 'hubert', '', 'https://jardinage.lemonde.fr/images/dossiers/categories3/racedecien-083123-650-325.jpg', '');

const chatgptBot = new Bot('chatgptBot', 'chatGPT', '', 'https://static.vecteezy.com/ti/vecteur-libre/p3/4996790-robot-chatbot-icon-sign-gratuit-vectoriel.jpg', '');

const michelBot = new Bot('michelBot', 'Michel', '', 'https://media.licdn.com/dms/image/C4E03AQGR-pXexSxhaw/profile-displayphoto-shrink_800_800/0/1603792556310?e=2147483647&v=beta&t=rzX4Ts2INrNUBJvoGj0TpMShL0PAowGVfjCXeNUIh_s', '');

const Demo = class {
  constructor() {
    this.sendbar = document.querySelector('#sendbar');
    this.el = document.querySelector('#app');
    this.title = 'Bonjour';
    this.boutonEnterMessage = document.getElementById('inputMSG');
    this.boutonEnterMessage.addEventListener('keypress', this.isEnter.bind(this), false);
    this.boutonClickMessage = document.getElementById('sendMSG');
    this.boutonClickMessage.addEventListener('click', () => {
      this.isClick();
    }, false);
  }

  run() {
    // this.sendbar.innerHTML = this.renderSendBar();
  //  this.el.innerHTML = this.render();
    this.waitAndResponce(michelBot, 'intro', true);
    this.getAllItems().forEach((item) => {
      if (item !== null) {
        this.sendMsg(item.text, true);
      }
    });
  }

  getAllItems() {
    const items = JSON.parse(localStorage.getItem(STORAGE_KEY)) || [];
    return items;
  }

  addItem(item) {
    const items = this.getAllItems();
    items.push(item);
    localStorage.setItem(STORAGE_KEY, JSON.stringify(items));
  }

  deleteAllItem() {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(''));
  }

  setLastSeen() {
    const date = new Date();
    const lastSeen = document.getElementById('lastseen');
    lastSeen.innerHTML = `last seen today at ${date.getHours()}:${date.getMinutes()}`;
  }

  findWord = (word, phrase) => {
    const words = phrase.split(' ');
    return words.includes(word);
  };

  findWordInArrayAndReturn = (word, array) => {
    const foundWord = array.find((item) => item === word);
    return foundWord || null;
  };

  findMatchingWord = (phrase, wordList) => {
    const wordsInPhrase = phrase.split(' ');
    return wordsInPhrase.find((word) => wordList.includes(word));
  };

  deleteWord(phrase, wordToDelete) {
    const mots = phrase.split(' ');
    const newWord = mots.filter((word) => word !== wordToDelete);
    const newPhrase = newWord.join(' ');
    return newPhrase;
  }

  sendTextMessage(bot, textToSend) {
    if (bot) {
      const date = new Date();
      setTimeout(this.setLastSeen, 1000);
      const myPic = document.createElement('img');
      myPic.setAttribute('src', `${bot.getImage}`);
      myPic.setAttribute('style', 'margin-bottom: 5%;margin-right: 60%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;');
      const myLI = document.createElement('li');
      const myDiv = document.createElement('div');
      const greendiv = document.createElement('div');
      const dateLabel = document.createElement('label');
      dateLabel.setAttribute('id', 'sentlabel');
      dateLabel.id = 'sentlabel';
      dateLabel.innerText = `${bot.getName} - ${date.getHours()}:${date.getMinutes()}`;
      myDiv.setAttribute('class', 'received');
      greendiv.setAttribute('class', 'grey');
      greendiv.innerHTML = `${textToSend}</br>`;
      greendiv.prepend(myPic);
      myDiv.appendChild(greendiv);
      myLI.appendChild(myDiv);
      greendiv.appendChild(dateLabel);
      document.getElementById('listUL').appendChild(myLI);
      const s = document.getElementById('chatting');
      s.scrollTop = s.scrollHeight;
    }
  }

  waitAndResponce(bot, inputText, isFirstTime) {
    if (inputText) {
      let date;
      const toDay = new Date();
      let newDate;
      let botInput;
      const arrayBot = ['michel', 'hubert', 'chatgpt', 'all'];
      if (bot === undefined) {
        botInput = this.findMatchingWord(inputText.toLowerCase(), arrayBot);
        if (botInput === undefined) {
          botInput = 'default';
        }
        switch (botInput.toLowerCase().trim()) {
          case 'michel':
            this.waitAndResponce(michelBot, inputText, isFirstTime);
            break;
          case 'hubert':
            this.waitAndResponce(hubertBot, inputText, isFirstTime);
            break;
          case 'chatgpt':
            setTimeout(() => {
              chatgptBot.generateChatGPT(inputText).then((text) => {
                this.sendTextMessage(chatgptBot, text);
              });
            }, 2000);
            break;
          case 'all':
            this.waitAndResponce(michelBot, inputText, isFirstTime);
            this.waitAndResponce(hubertBot, inputText, isFirstTime);
            this.waitAndResponce(chatgptBot, inputText, isFirstTime);
            break;
          default:
            if (inputText !== 'clear') {
              this.waitAndResponce(michelBot, 'who', true);
            } else {
              document.getElementById('listUL').innerHTML = '';
              this.deleteAllItem();
              this.waitAndResponce(bot, 'intro', true);
            }

            break;
        }
      }

      const array = ['intro', 'help', 'hi', 'hello', 'date', 'time', 'hey', 'name', 'joke', 'who', 'cocktail', 'openai'];
      let wordInput = this.findMatchingWord(inputText.toLowerCase(), array);
      const lastSeen = document.getElementById('lastseen');
      lastSeen.innerText = 'typing...';
      if (wordInput === undefined) {
        wordInput = 'default';
      }
      if (wordInput !== 'intro') {
        if (isFirstTime === false) {
          if (bot !== undefined) {
            // eslint-disable-next-line no-param-reassign
            bot.text = inputText;
            // eslint-disable-next-line no-param-reassign
            bot.date = `${toDay.getDate()}/${toDay.getMonth()}/${toDay.getFullYear()}- ${toDay.getHours()}:${toDay.getMinutes()}`;
            this.addItem(bot);
          }
        }
      }
      switch (wordInput.toLowerCase().trim()) {
        case 'intro':

          this.sendTextMessage(bot, "Hello there,<br><br>We are three bot :  <span class='bold'><a class='alink'>Michel</a></span>,<span class='bold'><a>ChatGPT</a></span> and <span class='bold'><a>Hubert </a></span>.</span><br><br>We are here to talk ! <br>Start with 'michel help' if you need help <br><br> you can write 'all' to call everyone <br><br> Ask for a 'cocktail' to someone to get a ramdom drink ! <br><br> Use the word 'openai' to call chatGPT with someone or all");
          break;
        case 'who':

          this.sendTextMessage(bot, "Hello there,<br><br>We are three bot :  <span class='bold'><a class='alink'>Michel</a></span>,<span class='bold'><a>ChatGPT</a></span> and <span class='bold'><a>Hubert </a></span>.</span><br><br> Try to rephrase the sentence by calling one of us");
          break;
        case 'help':
          this.sendTextMessage(bot, "<span class='sk'>Send Keyword to get what you want to know about me...<br> The idea is to call a robot by its first name and then say a word to it from the following list. The order doesn't matter.<br> example :  <br><span class='bold'>michel</span> tell me a joke<br> or <br> I really need a joke <span class='bold'>hubert</span> <br> 'hi', 'hello', 'date', 'time', 'hey', 'joke' <br><span class='bold'>'clear'</span> - to clear conversation<br>");
          break;
        case 'time':
          date = new Date();
          this.sendTextMessage(bot, `Current time is ${date.getHours()}:${date.getMinutes()}`);
          break;
        case 'date':
          newDate = new Date();
          this.sendTextMessage(bot, `Current date is ${newDate.getDate()}/${newDate.getMonth()}/${newDate.getFullYear()}`);
          break;
        case 'michel':
          this.sendTextMessage(bot, 'hello, ');
          break;

        case 'hello':
          this.sendTextMessage(bot, 'Hello there 👋🏻');
          break;

        case 'hi':
          this.sendTextMessage(bot, 'Hello there 👋🏻');
          break;

        case 'hey':
          this.sendTextMessage(bot, 'Hello there 👋🏻');
          break;
        case 'name':
          this.sendTextMessage(bot, 'What a coincidence!');
          this.sendTextMessage(bot, 'Hello! How are you?');
          break;
        case 'cocktail':
          if (bot) {
            setTimeout(() => {
              bot.fetchCocktailRecipe().then((text) => {
                this.sendTextMessage(bot, text.drinks[0].strDrink);
                this.sendTextMessage(bot, `Measure :  ${text.drinks[0].strMeasure1} - ${text.drinks[0].strMeasure2} -${text.drinks[0].strMeasure3} -${text.drinks[0].strMeasure4} - ${text.drinks[0].strMeasure5}`);
                this.sendTextMessage(bot, text.drinks[0].strInstructions);
              });
            }, 2000);
          }
          break;

        case 'joke':
          if (bot) {
            setTimeout(() => {
              bot.fetchJoke().then((text) => {
                this.sendTextMessage(bot, text.setup);
                this.sendTextMessage(bot, text.delivery);
              });
            }, 2000);
          }
          break;
        case 'openai':
          if (bot) {
            this.deleteWord(inputText, 'openai');
            setTimeout(() => {
              bot.generateChatGPT(inputText).then((text) => {
                this.sendTextMessage(bot, text);
              });
            }, 2000);
          }
          break;
        default:
          this.sendTextMessage(bot, "Sorry, don't understand");
      }
    }
  }

  sendMsg(event, isFirstTime) {
    const input = event;
    let htmlValue;
    let ti;
    if (input.value !== undefined) {
      htmlValue = input.value;
    }
    if (htmlValue === undefined) {
      ti = event;
    } else {
      ti = htmlValue;
    }

    const date = new Date();
    const myLI = document.createElement('li');
    const myDiv = document.createElement('div');
    const greendiv = document.createElement('div');
    const dateLabel = document.createElement('label');
    dateLabel.innerText = `${date.getHours()}:${date.getMinutes()}`;
    myDiv.setAttribute('class', 'sent');
    greendiv.setAttribute('class', 'green');
    dateLabel.setAttribute('class', 'dateLabel');
    greendiv.innerText = ti;
    myDiv.appendChild(greendiv);
    myLI.appendChild(myDiv);
    greendiv.appendChild(dateLabel);
    document.getElementById('listUL').appendChild(myLI);
    const s = document.getElementById('chatting');
    s.scrollTop = s.scrollHeight;
    this.waitAndResponce(undefined, ti, isFirstTime);
    document.getElementById('inputMSG').value = '';
  }

  isEnter(event) {
    if (event.keyCode === 13) {
      const data = document.getElementById('inputMSG');
      this.sendMsg(data, false);
    }
  }

  isClick() {
    const data = document.getElementById('inputMSG');
    this.sendMsg(data, false);
  }

  sendResponse() {
    setTimeout(this.setLastSeen, 100);
    const date = new Date();
    const myLI = document.createElement('li');
    const myDiv = document.createElement('div');
    const greendiv = document.createElement('div');
    const dateLabel = document.createElement('label');
    dateLabel.innerText = `${date.getHours()}:${date.getMinutes()}`;
    myDiv.setAttribute('class', 'received');
    greendiv.setAttribute('class', 'grey');
    dateLabel.setAttribute('class', 'dateLabel');
    greendiv.innerText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. ";
    myDiv.appendChild(greendiv);
    myLI.appendChild(myDiv);
    greendiv.appendChild(dateLabel);
    document.getElementById('listUL').appendChild(myLI);
    const s = document.getElementById('chatting');
    s.scrollTop = s.scrollHeight;
  }
};

const demo = new Demo();

demo.run();
